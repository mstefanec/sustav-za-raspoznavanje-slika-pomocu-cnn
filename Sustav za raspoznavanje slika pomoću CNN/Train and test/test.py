import os
import numpy as np
import keras
from keras.preprocessing import image

model = keras.models.load_model('model')
dogs=0
cats=0

test_files = os.listdir('testdata/')
for img in range(len(test_files)):
    test_image = image.load_img('testdata/' + test_files[img],
                                target_size=(64, 64))
    test_image = image.img_to_array(test_image)
    test_image = np.expand_dims(test_image, axis=0)
    result = model.predict(test_image)
    if result[0][0] >= 0.5:
        prediction = 'dog'
        probability = result[0][0]
        dogs+=1
        print("Prediction = " + prediction + (" probability: " + str(probability)))

    else:
        prediction = 'cat'
        probability = 1 - result[0][0]
        cats+=1
        print("Prediction = " + prediction + (" probability: " + str(probability)))

print("Number of dogs: " + str(dogs))
print("Number of cats: " + str(cats))

